package Han.Zammy;


import Han.Zammy.data.Locations;
import Han.Zammy.impl.*;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

import java.awt.*;

import static Han.Zammy.data.Settings.winesGrabbed;
import static Han.Zammy.data.Settings.winesMissed;

@ScriptMeta(developer = "Han", desc = "It's drunk, fam", name = "Zammy", version = 1)
public class Zammy extends TaskScript implements RenderListener {
    private static final Task[] TASKS = {new Teleport(),new Climb(),new Traverse(),new Banking(),new Casting(),};
    public static String zammyWine = "Wine of zamorak";
    public static Locations SELECTED_LOCATION;
    private StopWatch startTime = StopWatch.start();
    @Override
    public void onStart() {
        new ZammyGUI().setVisible(true);
        submit(TASKS);
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        Graphics g = renderEvent.getSource();
        Graphics2D g2d = (Graphics2D) g;
        g2d.drawString("Zammy Wine Grabber v1.0",20,35);
        g2d.drawString("Runtime: " + startTime.toElapsedString(),20,55);
        g2d.drawString("Amount of Wine Grabbed: " + (winesGrabbed - winesMissed),20,75);
}
}
