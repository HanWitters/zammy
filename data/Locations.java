package Han.Zammy.data;

import org.rspeer.runetek.api.movement.position.Area;

public enum Locations {
    SAFE_ZONE(
            Area.rectangular(2944, 3371, 2946, 3369),
            Area.rectangular(2939, 3517, 2939, 3517),
            Area.rectangular(2939, 3517, 2939, 3517,1),
            "Chaos Temple Safe");

    private final Area bankPoint, beginPoint, castPoint;
    private final String locName;

    Locations(final Area bankPoint, Area beginPoint, Area castPoint, String LocName) {
        this.bankPoint = bankPoint;
        this.beginPoint = beginPoint;
        this.castPoint = castPoint;
        this.locName = LocName;
    }
    public Area getBankPoint() {return bankPoint;}
    public Area getBeginPoint() {return beginPoint;}
    public Area getCastPoint() {return castPoint;}
    public String getLocName() {return locName;}
}
