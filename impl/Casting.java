package Han.Zammy.impl;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static Han.Zammy.Zammy.SELECTED_LOCATION;
import static Han.Zammy.Zammy.zammyWine;
import static Han.Zammy.data.Settings.winesGrabbed;

public class Casting extends Task{
    @Override
    public boolean validate() {
        if(Players.getLocal().isMoving()) return false;
        if(Players.getLocal().isAnimating()) return false;
        if(Pickables.getNearest(zammyWine) == null) return false;
        return SELECTED_LOCATION.getCastPoint().contains(Players.getLocal());
    }

    @Override
    public int execute() {
        if (Inventory.contains("Law rune")) {
            Magic.cast(Spell.Modern.TELEKINETIC_GRAB, Pickables.getNearest(zammyWine));
            winesGrabbed++;
        }else{
            System.out.println("It's time to stop!");
            return -1;
        }
        Time.sleepUntil(()->Pickables.getNearest(zammyWine) == null,50,1000);
        return 300;
    }
}
