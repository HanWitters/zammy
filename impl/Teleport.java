package Han.Zammy.impl;

import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static Han.Zammy.Zammy.SELECTED_LOCATION;
import static Han.Zammy.Zammy.zammyWine;

public class Teleport extends Task {
    @Override
    public boolean validate() {
        if (Players.getLocal().isAnimating()) return false;
        return Inventory.getCount(zammyWine) == 26 && SELECTED_LOCATION.getCastPoint().contains(Players.getLocal());
    }

    @Override
    public int execute() {
        Magic.cast(Spell.Modern.FALADOR_TELEPORT);
        return 275;
    }
}
