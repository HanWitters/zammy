package Han.Zammy.impl;

import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static Han.Zammy.Zammy.SELECTED_LOCATION;
import static Han.Zammy.Zammy.zammyWine;

public class Banking extends Task {
    @Override
    public boolean validate() {
        if(Inventory.getCount(zammyWine) <= 25 )return false;
        return SELECTED_LOCATION.getBankPoint().contains(Players.getLocal());
    }

    @Override
    public int execute() {
        if (!Bank.isOpen()){
            Bank.open();
            if (Inventory.contains(zammyWine)){
                System.out.println("Dropping tha loot!");
                Bank.depositAll(zammyWine);
            }
            if (!Inventory.contains("Law rune")){
                System.out.println("It's time to stop! Bye bye!");
                return -1;
            }
            if (!Inventory.contains("Water rune")){
                System.out.println("It's time to stop! Bye bye!");
                return -1;
            }
        }
        return 100;
    }
}
