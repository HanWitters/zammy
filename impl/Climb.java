package Han.Zammy.impl;

import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import static Han.Zammy.Zammy.SELECTED_LOCATION;

public class Climb extends Task {
    @Override
    public boolean validate() {
        return SELECTED_LOCATION.getBeginPoint().contains(Players.getLocal());
    }

    @Override
    public int execute() {
        SceneObjects.getNearest("Ladder").interact("Climb");
        return 300;
    }
}
