package Han.Zammy.impl;


import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static Han.Zammy.Zammy.SELECTED_LOCATION;
import static Han.Zammy.Zammy.zammyWine;

public class Traverse extends Task {
    @Override
    public boolean validate() {
        if(Players.getLocal().isMoving()) return false;
        return traverseToBank() || traverseToBegin();
    }

    @Override
    public int execute() {
        Movement.walkTo(traverseToBank() ? SELECTED_LOCATION.getBankPoint().getCenter().translate(Random.nextInt(-1,+1),Random.nextInt(-1,+1)) : SELECTED_LOCATION.getBeginPoint().getCenter());
        return 300;
    }
    private boolean traverseToBank(){
        return Inventory.getCount(zammyWine) == 26 && !SELECTED_LOCATION.getBankPoint().contains(Players.getLocal());
    }
    private boolean traverseToBegin(){
        return Inventory.getCount(zammyWine) < 1 && !SELECTED_LOCATION.getBeginPoint().contains(Players.getLocal()) && !SELECTED_LOCATION.getCastPoint().contains(Players.getLocal());
    }
}
