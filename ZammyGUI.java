package Han.Zammy;


import Han.Zammy.Zammy;
import Han.Zammy.data.Locations;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ZammyGUI extends JFrame {
    private JComboBox<Locations> locationComboBox;
    private JButton initiate;
    public ZammyGUI() {
        super("Lit Configuration");

        setLayout(new FlowLayout());
        initiate = new JButton("Drunk");

        locationComboBox = new JComboBox<>(Locations.values());
        add(locationComboBox);
        add(initiate);
        setLocationRelativeTo(null);
        initiate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Zammy.SELECTED_LOCATION = (Locations) locationComboBox.getSelectedItem();
                setVisible(false);
            }
        });
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();
    }
}
